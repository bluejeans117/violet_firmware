# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/violet/firmware/BTFM.bin:install/firmware/BTFM.bin \
    vendor/xiaomi/violet/firmware/NON-HLOS.bin:install/firmware/NON-HLOS.bin \
    vendor/xiaomi/violet/firmware/abl.elf:install/firmware/abl.elf \
    vendor/xiaomi/violet/firmware/aop.mbn:install/firmware/aop.mbn \
    vendor/xiaomi/violet/firmware/cmnlib.mbn:install/firmware/cmnlib.mbn \
    vendor/xiaomi/violet/firmware/cmnlib64.mbn:install/firmware/cmnlib64.mbn \
    vendor/xiaomi/violet/firmware/devcfg.mbn:install/firmware/devcfg.mbn \
    vendor/xiaomi/violet/firmware/dspso.bin:install/firmware/dspso.bin \
    vendor/xiaomi/violet/firmware/hyp.mbn:install/firmware/hyp.mbn \
    vendor/xiaomi/violet/firmware/imagefv.elf:install/firmware/imagefv.elf \
    vendor/xiaomi/violet/firmware/km4.mbn:install/firmware/km4.mbn \
    vendor/xiaomi/violet/firmware/multi_image.mbn:install/firmware/multi_image.mbn \
    vendor/xiaomi/violet/firmware/qupv3fw.elf:install/firmware/qupv3fw.elf \
    vendor/xiaomi/violet/firmware/storsec.mbn:install/firmware/storsec.mbn \
    vendor/xiaomi/violet/firmware/tz.mbn:install/firmware/tz.mbn \
    vendor/xiaomi/violet/firmware/uefi_sec.mbn:install/firmware/uefi_sec.mbn \
    vendor/xiaomi/violet/firmware/xbl.elf:install/firmware/xbl.elf \
    vendor/xiaomi/violet/firmware/xbl_config.elf:install/firmware/xbl_config.elf
